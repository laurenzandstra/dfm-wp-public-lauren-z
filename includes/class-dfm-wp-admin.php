<?php
/**
 * The admin functionality of the plugin.
 *
 * @package DFM_WP_Public
 */

if ( ! class_exists( 'DFM_WP_Admin' ) ) {
	/**
	 * Class DFM_WP_Admin
	 * Creates admin pages.
	 */
	class DFM_WP_Admin {

		/**
		 * DFM_WP_Admin constructor.
		 *
		 * @param string $category Slug of category.
		 * @param string $menu_title Title of menu.
		 * @param string $menu_slug Slug of menu page.
		 * @param int    $number_posts Number of posts per page for query.
		 * @param string $page_title Title of menu page.
		 * @param string $icon Dashicon for menu page.
		 * @param int    $position Position of new menu page in admin.
		 * @access public
		 */
		public function __construct( $category, $menu_title, $menu_slug, $number_posts = 10, $page_title = '', $icon = 'dashicons-admin-page', $position = 24 ) {
			$this->category     = $category;
			$this->menu_title   = $menu_title;
			$this->menu_slug    = $menu_slug;
			$this->page_title   = $page_title;
			$this->icon         = $icon;
			$this->number_posts = $number_posts;
			$this->position     = $position;

			add_menu_page( $this->page_title, $this->menu_title, 'edit_posts', $this->menu_slug, array( $this, 'render_page_content' ), $this->icon, $this->position );
		}

		/**
		 * Renders the admin page content.
		 *
		 * @access public
		 */
		public function render_page_content() {
			echo '<div class="wrap"><h2>' . esc_html( $this->menu_title ) . '</h2>';
			$args = array(
				'post_type'      => 'post',
				'post_status'    => 'publish',
				'no_found_rows'  => true,
				'cache_results'  => false,
				'posts_per_page' => $this->number_posts,
				'category_name'  => $this->category,
			);

			$the_query = new WP_Query( $args );

			if ( $the_query->have_posts() ) {
				echo '<ul style="padding-left: 20px; list-style: disc;">';
				while ( $the_query->have_posts() ) {
					$the_query->the_post();
					echo '<li>' . esc_html( get_the_title() ) . '</li>';
				}
				echo '</ul>';
			} else {
				echo '<div style="margin: 13px 0;">There are no ' . esc_html( $this->page_title ) . ' posts. Check back later.</div>';
			}

			wp_reset_postdata();
		}
	}
}
