<?php
/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
class DFM_WP_Public {

	/**
	 * The string used to uniquely identify this plugin.
	 *
	 * @since 1.0.0
	 * @access public
	 * @var string
	 */
	public $plugin_name;

	/**
	 * The plugin_url
	 *
	 * @since 1.0.0
	 * @access public
	 * @var string
	 */
	public $plugin_url;

	/**
	 * The plugin_path
	 *
	 * @since 1.0.0
	 * @access public
	 * @var string
	 */
	public $plugin_path;

	/**
	 * Current version of the plugin
	 *
	 * @since 1.0.0
	 * @access public
	 * @var string
	 */
	public $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name, url, path and plugin version that can be used throughout the plugin.
	 *
	 * @since 1.0.0
	 * @access public
	 */
	public function __construct( $plugin_root ) {
		$this->plugin_name = 'dfm-wp-public';
		$this->plugin_url = plugin_dir_url( $plugin_root );
		$this->plugin_path = plugin_dir_path( $plugin_root );
		$this->version = '1.0.0';
	}

	/**
	 * Register the actions and filters
	 *
	 * @since 1.0.0
	 * @access public
	 * @return void
	 */
	public function run() {

		// include dependencies.
		add_action( 'after_setup_theme', array( $this, 'setup_dependencies' ), 20, 0 );
		add_action( 'admin_menu', array( $this, 'add_menu_pages' ) );

	}

	/**
	 * Include dependencies
	 *
	 * @since 1.0.0
	 * @access public
	 * @return void
	 */
	public function setup_dependencies() {
		if ( is_admin() ) {
			// Include dependencies.
			require_once $this->plugin_path . 'includes/class-dfm-wp-admin.php';
		}
	}

	/**
	 * Adds the menu item for the transient control dashboard
	 *
	 * @access public
	 * @return void
	 */
	public function add_menu_pages() {

		$sports               = new stdClass();
		$sports->title        = 'Sports';
		$sports->menu_title   = 'Sports Content';
		$sports->menu_slug    = 'sports_content';
		$sports->icon         = 'dashicons-star-filled';
		$sports->number_posts = 25;
		$sports->category     = 'sports';

		$animals               = new stdClass();
		$animals->title        = 'Animals';
		$animals->menu_title   = 'Animals Content';
		$animals->menu_slug    = 'animals_content';
		$animals->icon         = 'dashicons-buddicons-replies';
		$animals->number_posts = 10;
		$animals->category     = 'animals';

		$business               = new stdClass();
		$business->title        = 'Business';
		$business->menu_title   = 'Business Content';
		$business->menu_slug    = 'business_content';
		$business->icon         = 'dashicons-portfolio';
		$business->number_posts = 12;
		$business->category     = 'business';

		$entertainment               = new stdClass();
		$entertainment->title        = 'Entertainment';
		$entertainment->menu_title   = 'Entertainment Content';
		$entertainment->menu_slug    = 'entertainment_content';
		$entertainment->icon         = 'dashicons-buddicons-groups';
		$entertainment->number_posts = 50;
		$entertainment->category     = 'entertainment';

		$news               = new stdClass();
		$news->title        = 'World and News';
		$news->menu_title   = 'World and News Content';
		$news->menu_slug    = 'news_content';
		$news->icon         = 'dashicons-admin-site';
		$news->number_posts = 100;
		$news->category     = 'news';

		$pages = array( $sports, $animals, $business, $entertainment, $news );
		foreach ( $pages as $page ) {
			$admin_page = new DFM_WP_Admin( $page->category, $page->menu_title, $page->menu_slug, $page->number_posts, $page->title, $page->icon );
		}
	}

}
